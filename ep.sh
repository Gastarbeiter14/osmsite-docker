#!/bin/bash
cd ~/openstreetmap-website
#export RAILS_ENV=production
export PGPASSWORD=osm
test set || {touch config/settings.local.yml && sleep 10 && bundle exec rake db:create && psql -h db -d openstreetmap -c "CREATE EXTENSION btree_gist" && psql -h db -d openstreetmap -f db/functions/functions.sql && bundle exec rake db:migrate &&  touch set }
bundle exec rails server

