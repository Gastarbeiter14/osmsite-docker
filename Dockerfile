FROM ubuntu:20.04
RUN apt-get update
RUN apt-get install -y apt-utils
ENV DEBIAN_FRONTEND="noninteractive"
RUN apt-get install -y tzdata 
RUN apt-get install -y ruby2.7 libruby2.7 ruby2.7-dev bundler libmagickwand-dev libxml2-dev libxslt1-dev nodejs apache2 apache2-dev build-essential git-core firefox-geckodriver libpq-dev libsasl2-dev imagemagick libffi-dev libgd-dev libarchive-dev libbz2-dev postgresql-client-12 npm nodejs yarnpkg
RUN gem2.7 install bundler:1.17.2 && \
	npm install -g svgo && \
	#ln -s /usr/bin/nodejs /usr/bin/node && \
	echo "osm ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers.d/osm && \
	useradd -m -s /bin/bash osm
USER osm
WORKDIR /home/osm
RUN git clone --depth=1 https://github.com/openstreetmap/openstreetmap-website.git
WORKDIR openstreetmap-website
ENV RAILS_ENV="production"
ENV RAILS_SERVE_STATIC_FILES=true
RUN echo 'gem "image_optim", "~> 0.25"'>> Gemfile && echo  'gem "image_optim_pack", "= 0.2.3"' >> Gemfile
RUN bundle install
RUN bundle exec rake yarn:install
COPY --chown=osm:osm database.yml config/
COPY --chown=osm:osm storage.yml config/
RUN touch config/settings.local.yml && rake i18n:js:export assets:precompile
WORKDIR /home/osm
COPY --chown=osm:osm ep.sh .
RUN chmod +x ep.sh
ENTRYPOINT /home/osm/ep.sh

#RUN sudo service postgresql start  && sudo -u postgres createdb -E UTF8 -O osm openstreetmap  && psql -d openstreetmap -c "CREATE EXTENSION btree_gist" &&  psql -d openstreetmap -f db/functions/functions.sql && bundle exec rake db:migrate 
